@extends('layouts.app')

@section('content')
    <form action="{{route('store')}}" method="POST">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5">
                    <h1>Create New Contact</h1>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="body">Email</label>
                        <input type="email" name="email" id="email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" id="phone" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="phone2">Phone 2</label>
                        <input type="text" name="phone2" id="phone2" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" id="address" class="form-control" required>
                    </div>
                    <input type="submit" value="Submit" class="btn btn-primary">
                </div>
            </div>
        </div>
        @csrf
    </form>
@endsection

