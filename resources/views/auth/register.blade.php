@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        <form method="POST" id="registrationForm" action="{{ route('register') }}"
                              aria-label="{{ __('Register') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="card-number"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Card Number') }}</label>

                                <div class="col-md-6">
                                    <input id="card-number" type="text" class="form-control"
                                           name="card-number" size="20" data-stripe="number" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="card-month"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Card Month') }}</label>

                                <div class="col-md-6">
                                    <input id="card-month" type="text" class="form-control"
                                           name="card-month" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="card-year"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Card Year') }}</label>

                                <div class="col-md-6">
                                    <input id="card-year" type="text" class="form-control"
                                           name="card-year" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="card-cvc"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Card CVC') }}</label>

                                <div class="col-md-6">
                                    <input id="card-cvc" type="text" class="form-control"
                                           name="card-cvc" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" id="submit-button" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://js.stripe.com/v2/"></script>
    <script>
        $(document).ready(function () {
            $("#name").val("riyad");
            $("#email").val("riyadnoapara@gmails.com");
            $("#password").val("123456");
            $("#password-confirm").val("123456");
            $("#card-number").val("4242424242424242");
            $("#card-month").val("10");
            $("#card-year").val("19");
            $("#card-cvc").val("124");

            Stripe.setPublishableKey('{{env("STRIPE_KEY")}}');
            $('#registrationForm').submit(function(event){
                var number = $("#card-number").val();
                var cvc = $("#card-cvc").val();
                var month = $("#card-month").val();
                var year = $("#card-year").val();
                event.preventDefault();
                $("#submit-button").prop("disabled",true);
                Stripe.card.createToken({
                    number: number,
                    cvc: cvc,
                    exp_month: month,
                    exp_year: year
                },stripeResponseHandler);
            });
            function stripeResponseHandler(status,response){
                var $form = $('#registrationForm');
                if(response.error){
                    alert(response.error.message);
                    $("#submit-button").prop("disabled",false);
                }else{
                    var token = response.id;
                    $form.append($('<input type="hidden" name="stripeToken">').val(token));
                    // Submit the form:
                    $form.get(0).submit();
                }
            }
        });
    </script>
@endsection
